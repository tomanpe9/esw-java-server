# Server - Java

## Detailed Assignment 

Write an HTTP 1.1 protocol based server application, which will count unique words in data sent by clients. The server must meet the following requirements: 

* Clients send data using POST method with path /esw/myserver/data. Data are in plain text format encoded in UTF-8 and compressed by gzip method.
* The server counts unique words. On startup, the counter equals zero.
* The server keeps records of words sent by clients. The unique word counter is increased by one for each new word, which is not in records yet.
* If a GET request with path /esw/myserver/count arrives, the server will answer actual value of unique count and reset it. The value is transferred as a decadic number in UTF-8 encoding.
* The server must be able to handle a large number of simultaneously connected clients (approximately 100).

more details at 
https://cw.fel.cvut.cz/wiki/courses/b4m36esw/labs/servers

## Position and other info 

The performance of server will be measured by a test program, which will send a bigger amount of data, and then ask for the current value of unique words. Total time from the first POST request send to the counter value reception will be measured.

Solutions twice faster than example server below will be accepted. The reference solution is measured on a computer in the laboratory.

https://rtime.felk.cvut.cz/esw/server/auth/results/java/

## Moving files and starting server on ritchie

Server is called ritchie (more details in assignment)

    scp -r src tomanpe9@ritchie.ciirc.cvut.cz:

SSH to server

    ssh tomanpe9@ritchie.ciirc.cvut.cz

And starting server

    cd src/
    javac cz/peta/Main.java 
    java -cp . cz.peta.Main


## Testing server

For testing purposes locally:

    curl localhost:8080/esw/myserver/data --data-binary @<(echo first attempt|gzip)
    curl localhost:8080/esw/myserver/data --data-binary @<(echo second attempt|gzip)
    curl localhost:8080/esw/myserver/count --output -
