package cz.peta;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;


/**
 * Initial idea of server was found on https://stackoverflow.com/questions/3732109/simple-http-server-in-java-using-only-java-se-api#3732328
 */
public class Main {

    private static ConcurrentHashMap<String, Integer> words = new ConcurrentHashMap<>();

    public static void main(String[] args) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
        server.createContext("/esw/myserver/data", new MyDataHandler());
        server.createContext("/esw/myserver/count", new MyCountHandler());
        server.setExecutor(new ThreadPerTaskExecutor());
        server.start();
    }

    private static class MyCountHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = words.size() + "\n";
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    private static class MyDataHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            InputStream is = t.getRequestBody();
            handleBasic(is);
            String response = "";
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }

        void handleBasic(InputStream io) throws IOException {
            InputStream gzipStream = new GZIPInputStream(io);
            Reader decoder = new InputStreamReader(gzipStream, StandardCharsets.UTF_8);
            BufferedReader buffered = new BufferedReader(decoder);
            ArrayList<Character> chars = new ArrayList<>();
            char c;
            while (buffered.ready()) {
                while (buffered.ready()
                        && (!Character.isWhitespace(c = (char) buffered.read()))) {
                    chars.add(c);
                }
                if (chars.isEmpty()) {
                    continue;
                }
                char[] text = new char[chars.size()];
                for (int i = 0; i < chars.size(); i++) {
                    text[i] = chars.get(i);
                }
                words.putIfAbsent(new String(text), 0);
                chars = new ArrayList<>();
//                String[] arr = buffered.readLine().split("\\s+");
//                for (String word : arr) {
//                    String str = word.trim();
//                    if (str.isEmpty()) {
//                        continue;
//                    }
//                    words.putIfAbsent(str, 0);
//                }
            }
        }
    }

    private static class ThreadPerTaskExecutor implements Executor {
        private final ExecutorService pool;

        ThreadPerTaskExecutor() {
            int cores = Runtime.getRuntime().availableProcessors();
            pool = Executors.newFixedThreadPool(cores);
        }

        public void execute(Runnable r) {
            pool.execute(r);
        }
    }

}